<?php
include "fonctions.php";

if ($_REQUEST['operation'] == "connexion") {
    try {
        $data = json_decode($_REQUEST["lesdonnees"]);

        $login = $data[1];
        $pass = $data[2];

        $cnx = connexionPDO();
        $req = $cnx->prepare('select * from visiteur WHERE login="' . $login . '"  AND mdp="' . $pass . '"');
        $req->execute();

        if ($user = $req->fetch(PDO::FETCH_ASSOC)) {
            print("connexion%" . json_encode($user));
        } else {
            print("erreurconn%Identifiants incorrect");
        }

    } catch (PDOException $e) {
        print "erreurconn%" . $e->getMessage();
        die();
    }
}

if ($_REQUEST['operation'] == "envoiDesFrais") {
    try {
        $data = json_decode($_REQUEST["lesdonnees"]);

        $idVisiteur = $data[0];
        $date = $data[2] . '-' . $data[1];
        $nuitee = $data[3];
        $etape = $data[4];
        $km = $data[5];
        $repas = $data[6];

        $cnx = connexionPDO();
        $req = $cnx->prepare('select * from ficheFrais WHERE idVisiteur="' . $idVisiteur . '" AND date="' . $date . '"');
        $req->execute();

        if ($ficheFrais = $req->fetch(PDO::FETCH_ASSOC)) {
            $req2 = $cnx->prepare('UPDATE ficheFrais SET nuitee="' . $nuitee . '", etape="' . $etape . '", km="' . $km . '", repas="' . $repas . '" WHERE idVisiteur="' . $idVisiteur . '" AND date="' . $date . '" ');
            $req2->execute();
            print("ficheFraisUpdate%Vos fiches de frais ont bien été mises à jour");
        } else {
            $req2 = $cnx->prepare("INSERT INTO ficheFrais (idVisiteur, date, nuitee, etape, km, repas) VALUES ('$idVisiteur', '$date', '$nuitee', '$etape', '$km', '$repas')");
            $req2->execute();
            print("ficheFraisInsert%Votre fiche de frais de la période ". $date ." a été créée");
        }

    } catch (PDOException $e) {
        print "erreurconn%" . $e->getMessage();
        die();
    }
}

if ($_REQUEST['operation'] == "viderFraisHf") {

    try {
        $data = json_decode($_REQUEST['lesdonnees']);
        $idVisiteur = $data[0];
        $date = $data[1];
        $cnx = connexionPDO();
        $req = $cnx->prepare("DELETE FROM fraisHorsForfait WHERE periode='". $date ."' AND idVisiteur='". $idVisiteur ."'");
        $req->execute();
    } catch (PDOException $e) {
        print "erreurconn%" . $e->getMessage();
        die();
    }
}

if ($_REQUEST['operation'] == "fraisHf") {
    try {
        $data = json_decode($_REQUEST['lesdonnees']);
        $idVisiteur = $data[0];
        $date = $data[2].'-'.$data[1];
        $jour = $data[3];
        $montant = $data[4];
        $motif = $data[5];
        $cnx = connexionPDO();
        $req = $cnx->prepare("INSERT INTO fraisHorsForfait (idVisiteur, periode, jour, montant, motif) VALUES ('$idVisiteur', '$date', '$jour', '$montant', '".$motif."')");
        $req->execute();
    } catch (PDOException $e) {
        print "erreurconn%" . $e->getMessage();
        die();
    }
}


?>