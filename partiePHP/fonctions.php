<?php
    function connexionPDO(){
        $login="root";
        $mdp="";
        $bd="gsbandroid";
        $serveur="localhost:3308";
        try {
            $conn = new PDO("mysql:host=$serveur;dbname=$bd", $login, $mdp);
            return $conn;
        } catch (PDOException $e) {
            print "Erreur de connexion PDO ";
            die();
        }
    }
?>
