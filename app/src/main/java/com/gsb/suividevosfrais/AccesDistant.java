package com.gsb.suividevosfrais;

import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gsb.suividevosfrais.AccesHTTP;
import com.gsb.suividevosfrais.AsyncResponse;


public class AccesDistant implements AsyncResponse {
    final String SERVERADDR="http://192.168.1.12:8080/projets/GsbAndroid/serveurGsbAndroid.php";
    private MainActivity mainActivity;

    public AccesDistant(MainActivity main) {
        this.mainActivity = main;
    }

    /**
     * reponse du serveur par output
     * @param output
     */
    @Override
    public void processFinish(String output) {
        Log.d("serveur","********"+output);
        String[] message = output.split("%");
        if(message.length>1){
            if (message[0].equals("connexion")){

                try {
                    JSONObject info=new JSONObject(message[1]);
                    Log.d("json object", info.toString());
                    String id=info.getString("id");
                    String login=info.getString("login");
                    String mdp=info.getString("mdp");
                    String nom=info.getString("nom");
                    String prenom=info.getString("prenom");
                    mainActivity.setVisiteur(new Visiteur(id,login,mdp,nom,prenom));
                    Toast.makeText(mainActivity,"Vous êtes connecté "+ prenom, Toast.LENGTH_LONG).show();
                    mainActivity.envoiFrais();
                    mainActivity.envoiFraisHF();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            if (message[0].equals("ficheFraisUpdate")) {
                    String messageInfo = message[1];
                    Toast.makeText(mainActivity, messageInfo, Toast.LENGTH_LONG).show();
            }


            if (message[0].equals("ficheFraisInsert")) {
                String messageInfo = message[1];
                Toast.makeText(mainActivity, messageInfo.toString(), Toast.LENGTH_LONG).show();
            }


            if(message[0].equals("erreurconn")){
                Toast.makeText(mainActivity,message[1], Toast.LENGTH_LONG).show();
            }
        }




    }

    /**
     * envoi l'intitule de l'operation des données
     * @param operation
     * @param lesDonneesJSON
     */
    public void envoi(String operation, JSONArray lesDonneesJSON){
        Log.d("operation", operation);
        Log.d("donnees", lesDonneesJSON.toString());
        AccesHTTP accesDonnees = new AccesHTTP();
        accesDonnees.delegate = this; // le lien de délégation entre AccesDistant et AccesHTTP
        accesDonnees.addParam("operation", operation);
        accesDonnees.addParam("lesdonnees", lesDonneesJSON.toString());
        accesDonnees.execute(SERVERADDR);
    }
}
