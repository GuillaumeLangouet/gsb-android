package com.gsb.suividevosfrais;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

    // proprietes de la classe
    private static AccesDistant accesdistant ;
    private static Visiteur visiteur ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // recuperation des informations serialisees
        recupSerialize();
        // chargement des methodes evenementielles
        cmdMenu_clic(((Button) findViewById(R.id.cmdNuitee)), NuiteeActivity.class);
        cmdMenu_clic(((Button) findViewById(R.id.cmdEtape)), EtapeActivity.class);
        cmdMenu_clic(((Button) findViewById(R.id.cmdRepas)), RepasActivity.class);
        cmdMenu_clic(((Button) findViewById(R.id.cmdKm)), KmActivity.class);
        cmdMenu_clic(((Button) findViewById(R.id.cmdHf)), HfActivity.class);
        cmdMenu_clic(((Button) findViewById(R.id.cmdHfRecap)), HfRecapActivity.class);
        cmdTransfert_clic();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Recupere la serialisation si elle existe
     */
    private void recupSerialize() {
        Global.listFraisMois = (Hashtable<Integer, FraisMois>) Serializer.deSerialize(Global.filename, MainActivity.this);
        // si rien n'a ete recupere, il faut creer la liste
        if (Global.listFraisMois == null) {
            Global.listFraisMois = new Hashtable<Integer, FraisMois>();
        }
    }

    /**
     * Sur la selection d'un bouton dans l'activite principale ouverture de l'activite correspondante
     */
    private void cmdMenu_clic(Button button, final Class classe) {
        button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                // ouvre l'activite
                Intent intent = new Intent(MainActivity.this, classe);
                startActivity(intent);
            }
        });
    }

    /**
     * Cas particulier du bouton pour le transfert d'informations vers le serveur
     */
    private void cmdTransfert_clic() {
        ((Button) findViewById(R.id.cmdTransfert)).setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                // envoi les informations serialisees vers le serveur
                // en construction
                LayoutInflater inflater = getLayoutInflater();
                View alertlayout = inflater.inflate(R.layout.layout_login_dialog, null);
                final EditText login2 = (EditText) alertlayout.findViewById(R.id.txtUsername2);
                final EditText mdp2 = (EditText) alertlayout.findViewById(R.id.txtPassword2);
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setTitle("Veuillez vous authentifier !");
                alert.setView(alertlayout);
                alert.setCancelable(false);//permet de ne pas fermer l'alert dialog
                alert.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getBaseContext(), "Connexion Annulee", Toast.LENGTH_SHORT).show();
                    }
                });
                alert.setPositiveButton("Connexion", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String login=login2.getText().toString();
                        String mdp=mdp2.getText().toString();
                        visiteur = new Visiteur("1000", login, mdp, "connexion", "connexion");
                        accesdistant = new AccesDistant(MainActivity.this) ;
                        Log.d("objet envoye", visiteur.convertToJSONArray().toString());
                        accesdistant.envoi("connexion", visiteur.convertToJSONArray());
                    }
                });
                AlertDialog dialog = alert.create();
                dialog.show();


            }
        });
    }

    public void envoiFrais() {
        Set lesClefs =  Global.listFraisMois.keySet();
        for(Object uneclef:lesClefs) {
            FraisMois lesFrais = Global.listFraisMois.get(uneclef);
            ArrayList liste=new ArrayList();
            liste.add(visiteur.getId());
            liste.add(lesFrais.getAnnee());
            liste.add(lesFrais.getMois());
            liste.add(lesFrais.getNuitee());
            liste.add(lesFrais.getEtape());
            liste.add(lesFrais.getKm());
            liste.add(lesFrais.getRepas());
            JSONArray listeJson = new JSONArray(liste);
            accesdistant.envoi("envoiDesFrais", listeJson);
        }
    }

    public void viderFraishf(int annee, int mois) {
        String idVisiteur = visiteur.getId();
        String date = Integer.toString(mois)+'-'+Integer.toString(annee);
        ArrayList donnees = new ArrayList();
        donnees.add(idVisiteur);
        donnees.add(date);
        accesdistant.envoi("viderFraisHf", new JSONArray(donnees));
    }

    public void envoiFraisHF() {
        Set lesClefs = Global.listFraisMois.keySet();
        for(Object uneclef:lesClefs) {
            FraisMois lesFrais = Global.listFraisMois.get(uneclef);
            int annee = lesFrais.getAnnee();
            int mois = lesFrais.getMois();
            viderFraishf(annee, mois);
            for (FraisHf unFraisHf:lesFrais.getLesFraisHf()) {
                ArrayList listeHf = new ArrayList();
                listeHf.add(visiteur.getId());
                listeHf.add(annee);
                listeHf.add(mois);
                listeHf.add(unFraisHf.getJour());
                listeHf.add(unFraisHf.getMontant());
                listeHf.add(unFraisHf.getMotif());
                Log.d("un frais hf enovye", listeHf.toString());
                accesdistant.envoi("fraisHf", new JSONArray(listeHf));
            }
        }
    }

    // getter et setter sur Visiteur
    public static Visiteur getVisiteur() {
        return visiteur;
    }
    public static void setVisiteur(Visiteur visiteur) {
        MainActivity.visiteur = visiteur;
    }
}
