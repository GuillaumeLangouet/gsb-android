package com.gsb.suividevosfrais;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guillaume on 23/03/2017.
 */

public class Visiteur {

    private String id;
    private String login;
    private String mdp;
    private String nom;
    private String prenom;

    public Visiteur(String id, String login, String mdp, String nom, String prenom) {
        this.id = id;
        this.login = login;
        this.mdp = mdp;
        this.nom = nom;
        this.prenom = prenom;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getNom() { return nom; }

    public void setNom(String nom) { this.nom = nom; }

    public String getPrenom() { return prenom; }

    public void setPrenom(String prenom) { this.prenom = prenom; }


    /**
     * Cobersion de l'objet au format JSONArray
     * @return
     */
    public JSONArray convertToJSONArray () {
        List list = new ArrayList();
        list.add(id);
        list.add(login);
        list.add(mdp);
        return new JSONArray(list);
    }

}
